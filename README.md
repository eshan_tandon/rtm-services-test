# README #

Tests for RTM services. 

Configuration:
1)Import rtm-services-test / Claims Flow / ClaimsFlow.postman_collection.json to Postman.

2)Import the environment file.

3)Run the test runner.

The test calls the following services in sequence:

![Screen Shot 2017-01-29 at 10.06.14 PM.png](https://bitbucket.org/repo/BLrr9r/images/1931411509-Screen%20Shot%202017-01-29%20at%2010.06.14%20PM.png)

The parameters from the previous service are setup as environment variables.